'''
Created on 28 Mar 2022

@author: Fernando Penaherrera @UOL/OFFIS
'''
import unittest
from src.dwd_data_downloader.dwd_downloader import meteo_data_downloader
import os


class dwd_downloader_test(unittest.TestCase):


    def test_function(self):
        location = {"name": "Berlin",
            "id": "BER",
            "lat": 52.53,
            "lon": 3.38
            }

        meteo_data_path=meteo_data_downloader(location,year=2020)
        
        self.assertTrue(os.path.isfile(meteo_data_path))
         
            
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()