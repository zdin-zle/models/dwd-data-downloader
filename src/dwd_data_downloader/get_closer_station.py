'''
Created on 01.12.2021

@author: Fernando Penaherrera @UOL/OFFIS

'''
from copy import deepcopy
import pandas as pd



def distance(x_1, y_1, x_0, y_0):
    '''
    Get the distance between two points
    :param x_1: X1
    :param y_1: Y1
    :param x_0: X0
    :param y_0: X0
    '''
    return (x_1 - x_0) ** 2 + (y_1 - y_0) ** 2


def get_closest_station(data_file, city):
    '''
    Get the closes station to a city based on its coordinates
    :param data_file: TXT from DWD with the info on all stations
    :param city: Location dictionary with "lon" and "lat" keys
    '''
    with open(data_file, encoding='latin1') as data:
        first_line = data.readline()
    first_line = first_line.strip()
    columns = first_line.split(" ")
    dfr = pd.read_fwf(
        data_file,
        skiprows=2,
        encoding='latin1',
        header=None,
        names=columns,
        dtype=str
    )

    df_temp = deepcopy(dfr)
    df_temp["lat_int"] = [float(x) for x in df_temp["geoBreite"]]
    df_temp["lon_int"] = [float(x) for x in df_temp["geoLaenge"]]

    x_0 = city["lat"]
    y_0 = city["lon"]
    df_temp["pseudo_distance"] = [
        distance(
            x_1, y_1, x_0, y_0) for x_1, y_1 in zip(
                df_temp["lat_int"], df_temp["lon_int"])]

    closest = df_temp[df_temp[
        "pseudo_distance"] == df_temp["pseudo_distance"].min()]

    return closest.iloc[0]

