'''
Created on 01.12.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
meteo_params = {
    0: {"name": "air_temperature",
        "symbol": "TU",
        "params": ["RF_TU",  # relative Feuchte
                   "TT_TU", ]  # Lufttemperatur
        },
    1: {"name": "cloud_type",
        "symbol": "CS",
        "params": ["V_N", ]  # Bedeckungsgrad aller Wolken
        },
    2: {"name": "cloudiness",
        "symbol": "N",
        "params": ["V_N", ]  # Bedeckungsgrad aller Wolken
        },
    3: {"name": "dew_point",
        "symbol": "TD",
        "params": ["TD",  # Taupunktstemperatur
                   "TT", ]  # Temperatur der Luft in 2m Hoehe
        },
    4: {"name": "moisture",
        "symbol": "TF",
        "params": ["P_STD",  # Stundenwerte Luftdruck
                   "RF_STD",  # Stundenwerte der Relativen Feuchte
                   "TD_STD",  # Taupunkttemperatur in 2m Hoehe
                   "TF_STD",  # berechnete Stundenwerte der Feuchttemperatur
                   "TT_STD",  # Lufttemperatur in 2m Hoehe
                   "VP_STD", ]  # berechnete Stundenwerte des Dampfdruckes
        },
    5: {"name":
        "precipitation",
        "symbol": "RR",
        "params": ["R1", ]  # stdl. Niederschlagshoehe
        },
    6: {"name": "pressure",
        "symbol": "P0",
        "params": ["P",  # auf NN reduzierter Luftdruck
                   "P0", ]  # Luftdruck in Stationshoehe
        },
    7: {"name": "soil_temperature",
        "symbol": "EB",
        "params": ["V_TE002",  # Temperatur im Erdboden in 2 cm Tiefe
                   "V_TE005",  # Temperatur im Erdboden in 5 cm Tiefe
                   "V_TE010",  # Temperatur im Erdboden in 10 cm Tiefe
                   "V_TE020",  # Temperatur im Erdboden in 20 cm Tiefe
                   "V_TE050",  # Temperatur im Erdboden in 50 cm Tiefe
                   "V_TE100", ]  # Temperatur im Erdboden in 1 m Tiefe
        },
    8: {"name": "solar",
        "symbol": "ST",
        "params": ["FD_LBERG",  # Stundensumme der kurzwelligen diffusen Himmelsstrahlung
                   "FG_LBERG",  # Stundensumme der kurzwelligen Globalstrahlung
                   "SD_LBERG", ]  # Stundensumme der Sonnenscheindauer
        },
    #=========================================================================
    # 9: {"name":
    #     "sun", "symbol":
    #     "SD", "params": ["SD_SO", ]  # stdl. Sonnenscheindauer
    #     },
    #=========================================================================
    10: {"name": "wind",
         "symbol": "FF",
         "params": ["D",  # Windrichtung Messnetz 3
                    "F", ]  # Windgeschwindigkeit Messnetz 3
         },
    11: {"name": "wind_synop",
         "symbol": "F",
         "params": ["DD",  # Windrichtung uml.,windstill DD=00
                    "FF", ]  # 10-Min-Mittel der Windgeschwindigkeit
         },
}

if __name__ == '__main__':
    pass
