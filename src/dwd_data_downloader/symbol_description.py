'''
Created on 03.12.2021

@author: Fernando Penaherrera @UOL/OFFIS

A descriptions of the symbols found for the DWD data

Some measurements are duplicated
'''

data_description = {
    "RF_TU": {
        "name": "RelHumidity",
        "descr": "Relative Humidity",
        "unit": "%",
        "new_unit": "%"
    },
    "TT_TU": {
        "name": "AirTemperature",
        "descr": "Air Temperature",
        "unit": "C",
        "new_unit": "C"
    },
    "V_N": {
        "name": "CloudCoverage0",
        "descr": "Cloud Coverage",
        "unit": "1/8",
        "new_unit": "1/8"
    },
    "V_N": {
        "name": "CloudCoverage",
        "descr": "Cloud Coverage",
        "unit": "1/8",
        "new_unit": "1/8"
    },
    "TD": {
        "name": "DewPoint",
        "descr": "Air Dew Point",
        "unit": "C",
        "new_unit": "C"
    },
    "TT": {
        "name": "AirTemp2m",
        "descr": "Air Temperature at 2m",
        "unit": "C",
        "new_unit": "C"
    },
    "P_STD": {
        "name": "AirPressHourly",
        "descr": "Air Pressoure, Hourly",
        "unit": "hPa",
        "new_unit": "Pa"
    },
    "RF_STD": {
        "name": "RelHumidityHourly",
        "descr": "Relative Humidity, Hourly",
        "unit": "%",
        "new_unit": "%"
    },
    "TD_STD": {
        "name": "DewPoint2mHourly",
        "descr": "Dew Point Temperature, Hourly",
        "unit": "C",
        "new_unit": "C"
    },
    "TF_STD": {
        "name": "WetBulbTempHourly",
        "descr": "Wet Bulb Temperature, Hourly",
        "unit": "C",
        "new_unit": "C"
    },
    "TT_STD": {
        "name": "AirTemp2mHourly",
        "descr": "Air Temperature at 2m, Hourly",
        "unit": "C",
        "new_unit": "C"
    },
    "VP_STD": {
        "name": "VaporPressHourly",
        "descr": "Vapor Pressure, Hourly",
        "unit": "hPa",
        "new_unit": "Pa"
    },
    "R1": {
        "name": "RainHeight",
        "descr": "Precipitatioin Height",
        "unit": "mm",
        "new_unit": "mm"
    },
    "P": {
        "name": "SeaLvlAirPress",
        "descr": "Preassure at Sea level",
        "unit": "hPa",
        "new_unit": "Pa"
    },
    "P0": {
        "name": "AirPressStation",
        "descr": "Pressure at station Level",
        "unit": "hPa",
        "new_unit": "Pa"
    },
    "V_TE002": {
        "name": "SoilTemp2cm",
        "descr": "Soil Temperature at 2cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "V_TE005": {
        "name": "SoilTemp5cm",
        "descr": "Soil Temperature at 5cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "V_TE010": {
        "name": "SoilTemp10cm",
        "descr": "Soil Temperature at 10cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "V_TE020": {
        "name": "SoilTemp20cm",
        "descr": "Soil Temperature at 20cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "V_TE050": {
        "name": "SoilTemp50cm",
        "descr": "Soil Temperature at 50cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "V_TE100": {
        "name": "SoilTemp100cm",
        "descr": "Soil Temperature at 100cm depth",
        "unit": "C",
        "new_unit": "C"
    },
    "FD_LBERG": {
        "name": "DiffRadiation",
        "descr": "Diffuse Radiation",
        "unit": "J/cm2",
        "new_unit": "Wh/m2"
    },
    "FG_LBERG": {
        "name": "GlobalRadiation",
        "descr": "Global Horizontal Radiation",
        "unit": "J/cm2",
        "new_unit": "Wh/m2"
    },
    "SD_LBERG": {
        "name": "SunshineMins",
        "descr": "Sunshine Duration Minutes",
        "unit": "Minute",
        "new_unit": "Minute"
    },
    "SD_SO": {
        "name": "SunshineHours",
        "descr": "Sunshine Duration hours",
        "unit": "h",
        "new_unit": "h"
    },
    "D": {
        "name": "WindDir",
        "descr": "Wind Direction",
        "unit": "deg",
        "new_unit": "deg"
    },
    "F": {
        "name": "WindSpeed",
        "descr": "Wind Speed",
        "unit": "m/s",
        "new_unit": "m/s"
    },
    "DD": {
        "name": "WindDir10min",
        "descr": "Wind Direction, 10min avg",
        "unit": "deg",
        "new_unit": "deg"
    },
    "FF": {
        "name": "WindSpeed10min",
        "descr": "Wind Speed, 10min avg",
        "unit": "m/s",
        "new_unit": "m/s"
    },
    "00": {
        "name": "DirectNormalRad",
        "descr": "Direct Normal Radiation",
        "unit": "J/cm2",
        "new_unit": "Wh/m2"
    },
    "01": {
        "name": "BlackSkyTemp",
        "descr": "Blackbody Sky Temperature",
        "unit": "C",
        "new_unit": "C"
    }
}


if __name__ == '__main__':
    pass
