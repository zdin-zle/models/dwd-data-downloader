'''
Created on 01.12.2021

@author: Fernando Penaherrera @UOL/OFFIS
         Sara Fayed @HSEL

Downloader for the data from the DWD

This is a collection of methods to download data from the opendata.dwd.de
website.
1. All the meta data is downloaded to look for the "closest" location
with information available
2. The information of the selected station is downloaded and extracted
3. Information is filtered and restructured in dataframes
4. Extra calculated information is aggregated
5. CSV file (compatible with mosaik csv) and Description TXT are written
6. Temporal files are deleted
'''

from os import listdir, getcwd, remove
from os.path import isfile, join
from pvlib.solarposition import get_solarposition
from pvlib.irradiance import dni
from pip._vendor import requests
from bs4 import BeautifulSoup as bs
import urllib
import logging
import datetime
import pandas as pd
import zipfile
import numpy as np
from .data_types import meteo_params
from .get_closer_station import get_closest_station
from .symbol_description import data_description

# Logging information
logging.basicConfig(
    filename="MeteodataLog.log",
    filemode='a',
    format='%(asctime)s, %(name)s %(levelname)s %(message)s',
    datefmt='HH:MM:SS',
    level=logging.INFO)


def get_urls(meteo_params):
    """
    Builds the url String for downloading the meta-data of the DWD available
    stations.

    Reads the information from the meteo_params dict
    :param meteo_params: A dictionary with the following structure:
    ::
        0: {"name": "air_temperature",
            "symbol": "TU",
            "params": ["RF_TU",    # relative Feuchte
                       "TT_TU", ]  # Lufttemperatur
        ...
    :return: Dictionary with the urls string for each parameter
    """
    urls = {}

    for _, k in meteo_params.items():
        base_url = "https://opendata.dwd.de/climate_environment/CDC/" + \
            "observations_germany/climate/hourly/"
        if k["name"] not in ["solar"]:
            url = base_url + f"{k['name']}/historical/" + \
                f"{k['symbol']}_Stundenwerte_Beschreibung_Stationen.txt"

        elif k["name"] == "solar":
            url = base_url + f"{k['name']}/" +\
                f"{k['symbol']}_Stundenwerte_Beschreibung_Stationen.txt"

        urls[k["name"]] = url

    return urls


def download_description_urls(urls):
    '''
    Downloads the given description urls to the current working folder
    Files are TXT files with information on the available stations

    :param urls: Dictionary with the parameter name and the corresponding url.
    '''
    logging.info("Downloading DWD description files.")

    for name, url in urls.items():
        filename = join(getcwd(), f"TMP-description-{name}.txt")

        if not isfile(filename):
            try:
                urllib.request.urlretrieve(url, filename)
            except Exception:
                print(name, "URL does not exist on the Internet")
        else:
            logging.info("File exists, skipping download: " + filename)

    return None


def get_closest_stations_data(city, urls):
    '''
    Looks for the meteorological station with the closest "pseudo-distance"
    to the given location. Returns a dictionary with the information on the station

    :param city: Dictionary with the information of the location. ::
                city = {
                        "name": "Bremen",
                        "lat": 53.07,
                        "lon": 8.80, },
    :param urls: Dictionary with the parameter and url for the description

    :return: Dictionary with the stations data for the city. ::
            {'Braunschweig': {'air_temperature': {'from': '19510101',
                                              'station_id': '00662',
                                              'station_name': 'Braunschweig',
                                              'symbol': 'TU',
                                              'to': '20211208'},
                            ...
    '''
    stations_data = {city["name"]: {}}

    for name, _ in urls.items():
        file = join(getcwd(), f"TMP-description-{name}.txt")
        data = get_closest_station(file, city)
        res = {}
        res["station_id"] = data["Stations_id"]
        for v in meteo_params.values():
            if name == v["name"]:
                res["symbol"] = v["symbol"]
        res["from"] = data["von_datum"]
        res["to"] = data["bis_datum"]
        res["station_name"] = data["Stationsname"]
        stations_data[city["name"]][name] = res

    logging.info(f"Closest stations found for {city['name']}.")
    return stations_data


def build_weather_urls(stations_data):
    """
    Builds strings with the appropiate URLs to download data.
    Appends a key and value to the input stations data.
    If no data is found, the website is scrapped to get the url for
    the given station

    :param stations_data: Dictionary with the information on the stations
                          for each parameter. ::

    {'Braunschweig': {'air_temperature': {'from': '19510101',
                                          'station_id': '00662',
                                          'station_name': 'Braunschweig',
                                          'symbol': 'TU',
                                          'to': '20211130'},

    :return: Original dictionary with the "url" string field appended.
    """
    city = list(stations_data.keys())[0]
    # year = datetime.datetime.now().year - 1
    base_url = "https://opendata.dwd.de/climate_environment/" + \
        "CDC/observations_germany/climate/hourly/"
    for key, val in stations_data[city].items():
        if key == "solar":
            url_solar = f"/solar/stundenwerte_ST_{val['station_id']}_row.zip"
            url = base_url + url_solar
        else:

            id_url = f"{key}/historical/stundenwerte_" + \
                f"{val['symbol']}_{val['station_id']}_{val['from']}_{val['to']}_hist.zip"
            url = base_url + id_url
            response = requests.get(url)
            #==================================================================
            # if response.status_code == 404:
            #     id_url_2020 = "{}/historical/stundenwerte_{}_{}_{}_{}_hist.zip".format(
            #         key, val["symbol"], val["station_id"], val["from"], "{}1231".format(year))
            #     url = base_url + id_url_2020
            #     response = requests.get(url)
            #==================================================================

            if response.status_code == 404:
                # Here a scrapper for the website

                scrap_url = base_url + key + "/historical/"
                req = requests.get(scrap_url)
                soup = bs(req.content, 'html.parser')
                lista = soup.find_all("a", href=True)
                for list_e in lista:
                    if f"_{val['station_id']}_" in list_e["href"]:
                        zip_url = list_e["href"]

                url = scrap_url + zip_url

        stations_data[city][key]["url"] = url

    return stations_data


def download_meteo_data(stations_data):
    '''
    Downloads the ZIP files with the raw data for each station specified in the
    stations_data dictionary.

    :param stations_data: Dictionary containing the URL for the ZIP file.
    '''
    city = list(stations_data.keys())[0]
    stations = stations_data[city]
    i = 0
    for key, val in stations.items():
        file_name = join(getcwd(), f"TMP-{city}-{key}.zip")
        stations_data[city][key]["zip_file"] = file_name

        if not isfile(file_name):
            try:
                urllib.request.urlretrieve(val["url"], file_name)
                i += 1
            except Exception:
                stations_data[city][key]["zip_file"] = None
                logging.info(val["url"] + " not found.")

    logging.info(f"{i} ZIP files downloaded")

    return stations_data


def extract_data_from_zip(zip_filename, year):
    '''
    Extracts the relevant data from the downloaded zip file.
    Looks for the *.txt file that starts with "produkt" as per the DWD standards.
    Chooses the columns with relevant data, and filers according to dates
    Removes the *.txt file after processing

    :param zip_filename: Path to the downloaded *.zip file
    :param year: int of the year to download. Between 2010 and 2020.
    '''

    if year not in range(2010, 2021):
        raise ValueError("Year must be between 2010 and 2020")

    # Create a ZipFile Object and load sample.zip in it
    with zipfile.ZipFile(zip_filename) as zip_obj:

        # Get a list of all archived file names from the zip
        list_of_file_names = zip_obj.namelist()
        for file_name in list_of_file_names:
            if file_name.startswith(
                    'produkt'):  # Check filename starts with "produkt"
                txt_filename = join(getcwd(), file_name)
                zip_obj.extract(file_name, path=getcwd())

    # Read the txt file and process the dataframe
    data = pd.read_csv(txt_filename, sep=";")

    # Remove spaces from column names
    data.columns = data.columns.str.replace(' ', '')
    columns_list = list(data_description.keys())
    for col in columns_list:
        if col in data.columns:
            data = data.rename(columns={col: data_description[col]["name"]})

    # Assure that the 'MESS_DATUM' column only has date information.
    if isinstance(data['MESS_DATUM'][0], str):
        data['MESS_DATUM'] = [int(x[0:10]) for x in list(data['MESS_DATUM'])]

    # Build date time indexes
    data['Time'] = pd.to_datetime(
        data['MESS_DATUM'].astype(str),
        format='%Y%m%d%H')
    data = data.set_index('Time')

    # check which year is the first and last that contains a full year
    first_year = [x for x in data['MESS_DATUM'] if "0101" in str(x)[:10]][0]
    last_year = [x for x in data['MESS_DATUM'] if "1231" in str(x)[:10]][-1]
    first_year = str(first_year)[0:4]
    last_year = str(last_year)[0:4]
    first_year = int(first_year)
    last_year = int(last_year)
    range_years = range(first_year, last_year)

    # If the last year requested is not in range, look for the last year
    # available
    difference = 0
    if year not in range_years:
        difference = year - last_year
        year = last_year

    # Filter data only if in the columns_list dictionary
    new_columns = [data_description[col]["name"] for col in columns_list]
    cols = [col for col in data.columns if col in list(new_columns)]
    data = data[cols]

    # Filter by date ###
    start = f"{year}-01-01"
    end = f"{year}-12-31"
    data = data.loc[start:end]

    if difference > 0:
        data.index.shift(difference, freq="Y")
        logging.info(
            f"Data for {zip_filename} found for {year - difference} instead of {year}")

    if "cloud_type" in zip_filename:
        data = data.rename(columns={"CloudCoverage": "CloudCoverage0"})

    remove(txt_filename)

    data = data.loc[~data.index.duplicated(keep='first')]

    return data


def extract_data_from_all_zips(stations_data, year):
    '''
    Extracts data for all the ZIP files in the stations_data for the given year.
    Creates a dataframe with the results.

    :param stations_data: Dictionary with the ZIP files names
    :param year: int with the year for data download.

    :return: Dataframe with the meteorological data requested for a full year.
    '''
    city = list(stations_data.keys())[0]
    stations = stations_data[city]
    dfs = []
    for val in stations.values():
        if val["zip_file"] is not None:
            dfr = extract_data_from_zip(val["zip_file"], year)
            dfs.append(dfr)

    meteo_data = pd.concat(dfs, axis=1)
    logging.info("Data extracted from ZIP files")
    return meteo_data


def post_processs_data(city, meteo_data):
    '''
    Postsprocessing of the resulting meteodata. A chain of separate methods
    to clear, fill, convert, and add extra calculated columns.

    :param city: Dictionary with the information of the location. ::
                 "HB": {
                        "name": "Bremen",
                        "lat": 53.07,
                        "lon": 8.80, },
    :param meteo_data: Dataframe with meteorological data from the
                      extract_data_from_all_zips() method.

    :return: Dataframe with the required changes and added columns
    '''

    meteo_data = postprocess_data_remove_nan(meteo_data)
    meteo_data = postprocess_data_change_units(meteo_data)
    meteo_data = postprocess_data_add_calculated_columns(city, meteo_data)

    return meteo_data


def write_mosaik_csv_meteodata(meteo_data, year, city, path=None):
    '''
    Writes a CSV of the column of a dataframe and a separate TXT
    with the data description
    (Since mosaik cannot ignore lines that are full comments)

    The output files need to be in the MOSAIK-CSV format::

        Braunschweig
        Date,SolarRad,Pressure,WindSpeed
        01.01.2014 00:00,0,1003,6.1
        01.01.2014 01:00,0,1050,5.8
        ...

    Files are written in the output folder.

    :param meteo_data: Dataframe to be written to CSV
    :param year: Year of the data
    :param city: Dictionary with the city information
    :param path: Path to save the file. Writes to curennt path if non is given

    '''
    city = city["name"]

    if path is None:
        path = getcwd()

    filename = f"{city}_meteodata_{year}.csv"
    filename = join(path, filename)

    filename_descr = f"{city}_meteodata_{year}_description.txt"
    filename_descr = join(path, filename_descr)

    # Write the CSV file of with the column of data
    meteo_data.to_csv(filename, index=True)

    # Write the header so that it is readable with MOSAIK-CSV

    # Write the units as comments
    comments = ""
    for col in meteo_data.columns:
        for _, val in data_description.items():
            if col in val["name"]:
                comments += f"# {col}: {val['descr']} [{val['new_unit']}] \n".replace(
                    ",", ";")

    with open(filename_descr, 'w') as file:
        file.seek(0, 0)
        file.write(f"{city}_MeteoData_description" + '\n')
        file.write(comments + '\n')

    logging.info(f"File written: {filename_descr}")

    title = city + "_MeteoData"
    with open(filename, 'r+') as file:
        content = file.read()
        file.seek(0, 0)
        file.write(title + '\n' + content)

    logging.info(f"File written: {filename}")
    return filename


def postprocess_data_remove_nan(meteo_data):
    '''
    Removes -999 values and replaces them with nan
    Interpolates the values for the missing points using linear interpolation

    :param meteo_data: Dataframe resulting from the extract_data_from_all_zips() method.

    :return: Dataframe with the processed information
    '''
    # First, replace -999 with nan
    columns = meteo_data.columns
    meteo_data = meteo_data.replace(-999, np.nan)

    # Look for nan data and interpolate
    for col in columns:
        try:
            col_vals = meteo_data[col]
            ok_vals = -np.isnan(col_vals)
            xp_val = ok_vals.ravel().nonzero()[0]
            fp_val = col_vals[-np.isnan(col_vals)]
            x_val = np.isnan(col_vals).ravel().nonzero()[0]
            col_vals[np.isnan(col_vals)] = np.interp(x_val, xp_val, fp_val)
            meteo_data[col] = col_vals
        except Exception:
            logging.info("Could not correct NaN values for column " + col)

    return meteo_data


def postprocess_data_change_units(meteo_data):
    '''
    Converts units according to predefined values.
    >hPa to Pa
    >J/cm2 to Wh/m2

    :param meteo_data: Dataframe resulting from the extract_data_from_all_zips() method.
    :return: Dataframe with the processed information
    '''

    cols_pressure = [
        "VaporPressHourly",
        "AirPressHourly",
        "SeaLvlAirPress",
        "AirPressStation", ]
    cols_rad = [
        "DiffRadiation",
        "GlobalRadiation", ]

    for col in cols_pressure:
        if col in meteo_data.columns:
            meteo_data[col] = meteo_data[col] * 100  # hPa = 100*Pa

    for col in cols_rad:
        if col in meteo_data.columns:
            meteo_data[col] = meteo_data[col] * 1e4 / \
                3.6e3  # J/cm2 = 1e4/3.6e3 Wh/m2

    logging.info("Units converted: hPa to Pa ")
    logging.info("Units converted: J/cm2 to kWh/m2")
    return meteo_data


def postprocess_data_add_calculated_columns(city, meteo_data):
    '''
    Adds calculated columns for Direct Normal Irradiacion (DNI) and for
    Black Body Sky Temperature based on literature formulas.

    :param city: Dictionary with information of the location
    :param meteo_data: Dataframe resulting from the extract_data_from_all_zips() method.
    :return: Dataframe with the processed information

    '''
    # DNI Calculation
    # Based on the PVLib Methods. A non dependency would be much preferred.

    dhi = meteo_data["DiffRadiation"]
    ghi = meteo_data["GlobalRadiation"]
    dti = meteo_data.index

    # Solar position based on time, latitude and longitude
    solar_position = get_solarposition(dti, city["lat"], city["lon"])
    zenith = solar_position["zenith"]

    # calculate DNI
    dni_vals = dni(ghi, dhi, zenith)
    meteo_data["DirectNormalRad"] = dni_vals

    # Correct values for meteo cloud sky cover
    if "CloudCoverage" in meteo_data.columns:
        meteo_data["CloudCoverage"] = meteo_data["CloudCoverage"].clip(lower=0)
    if "CloudCoverage0" in meteo_data.columns:
        meteo_data["CloudCoverage0"] = meteo_data["CloudCoverage0"].clip(
            lower=0)

    # BlackBodySkyTemperature
    dry_bulb_temp = meteo_data["AirTemperature"] + 273.15
    dew_point_temp = meteo_data["DewPoint"] + 273.15
    opaque_sky_cover = meteo_data["CloudCoverage"] / 8
    tdew_poi_k = pd.concat([dry_bulb_temp, dew_point_temp], axis=1).min(axis=1)
    n_opa_10 = opaque_sky_cover  # Scaled to 10

    # http://www.ibpsa.org/proceedings/BS2019/BS2019_210351.pdf
    # Empirical formula for emissivity under clear sky based on Dew Point
    # Temperature
    emmisitivy = 0.711 + 0.56 * \
        (tdew_poi_k - 273.15) / 100 + 0.73 * ((tdew_poi_k - 273.15) / 100)**2

    # Santamouris, M. (2013). Energy and climate in the urban built environment.
    # Empirical formula for the sky emissivity based on cloud opacity
    eps_sky = emmisitivy * (1 + 0.0224 * n_opa_10 - 0.0035 *
                            (n_opa_10**2) + 0.00028 * (n_opa_10**3))

    # Equivalence through Stefan-Boltzmann Law
    t_bla_sky = dry_bulb_temp * (eps_sky**0.25)

    meteo_data["BlackSkyTemp"] = t_bla_sky - 273.15
    logging.info("Columns 'DirectNormalRad' and 'BlackSkyTemp' added")
    return meteo_data


def delete_files():
    '''
    Delete temporary files in the current working directory
    '''
    mypath = getcwd()
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

    # Select all ZIP and TXT
    files_to_delete = [
        file for file in onlyfiles if any(
            word in file for word in [
                ".zip", ".txt"])]

    # Except the ones named "_description.txt"
    files_to_delete = [
        f for f in files_to_delete if "_description.txt" not in f]

    files_to_delete = [
        f for f in files_to_delete if "TMP-" in f[0:4]]

    i = 0
    for file in files_to_delete:
        remove(file)
        i += 1
    logging.info(f"{i} temporal files deleted")

    return None


def meteo_data_downloader(
        location,
        year=2020,
        path=None,
        delete_temp_files=True):
    '''
    Main method collecting and chaining the different methods to download
    the data and save it into a CSV for MOSAIK-CSV

    Example:
    >>>    location = {
                       "name": "Berlin",
                       "id": "BER",
                       "lat": 52.53,
                       "lon": 3.38
                      }

    >>>    meteo_data_downloader(location,year=2020)

    :param location: Dictionary containing the location information. See example
    :param year: Year for data downloading
    :param path: Path to save the files.
    :param delete_temp_files: bool to delete the downloaded raw files:param year:

    '''
    urls = get_urls(meteo_params)
    download_description_urls(urls=urls)
    stations_data = get_closest_stations_data(city=location, urls=urls)
    stations_data = build_weather_urls(stations_data)
    stations_data = download_meteo_data(stations_data)
    meteo_data = extract_data_from_all_zips(stations_data, year)
    meteo_data = post_processs_data(city=location, meteo_data=meteo_data)

    if path is None:
        full_path = getcwd()
    else:
        full_path = join(getcwd(), path)

    meteo_data_path = write_mosaik_csv_meteodata(
        meteo_data, year, city=location, path=full_path)

    if delete_temp_files:
        delete_files()

    return meteo_data_path


if __name__ == '__main__':
    location = {"name": "Berlin",
                "id": "BER",
                "lat": 52.53,
                "lon": 3.38
                }

    meteo_data_downloader(location, year=2020)
