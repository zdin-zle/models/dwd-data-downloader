'''
Created on 22.02.2022

@author: Fernando Penaherrera @UOL/OFFIS
'''

"""
A setuptools based setup module.
See:
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils

from setuptools import setup, find_packages
import pathlib
here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file

long_description='\n\n'.join(
  open(f, 'rb').read().decode('utf-8')
  for f in ['README.rst', 'CHANGELOG', 'AUTHORS.txt']),



# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='dwd-data-downloader',  # Required
    version='0.1.1',  # Required
    description='A tool for downloading data from the DWD Website with a format to be used with MOSAIK-CSV',  # Optional
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://gitlab.com/zdin-zle/models/dwd-data-downloader',  # Optional
    author='Fernando Penaherrera V.',  # Optional
    author_email='fernandoandres.penaherreravaca@offis.de',  # Optional
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Researchers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',

    ],

    keywords='Meteorological Data, Weather Data, Mosaik, Adapter',  # Optional
    packages=find_packages(
        where="src",
        include=["dwd_data_downloader*"]

    ),
    package_dir={"": "src"},
    python_requires='>=3.6, <4',
    install_requires=[
        "pandas",
        "numpy",
        "beautifulsoup4",
        "pvlib==0.9.0"],

    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/zdin-zle/models/photovoltaic/-/issues',
        'Funding': 'ZLE Funders',
        'Documentation': "https://zdin.de/zukunftslabore/energie",
        'Source': 'https://gitlab.com/zdin-zle/models/dwd-data-downloader',
    },
)


if __name__ == '__main__':
    pass
