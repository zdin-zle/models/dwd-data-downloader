

===========
DWD Data Downloader
===========

**A tool for downloading meteorological data from DWD**

.. contents::
    :depth: 2
    :local:
    :backlinks: top

Description
===========

Prepares a CSV Database to be used with Mosaik-CSV. 
Uses data from the opendata.dwd.de to fetch data.
Filters data for a given year or for given date intervals.

Installation
===========
Currently an installation through pip + git is required.
::

	pip install git+https://gitlab.com/zdin-zle/models/dwd-data-downloader.git


After installation, the following are examples of how to create and data into csv files:

::

	from dwd_data_downloader.dwd_downloader import meteo_data_downloader
	
	location = {"name": "Berlin",
	    		"id": "BER",
	    		"lat": 52.53,
	    		"lon": 3.38}
	meteo_data_path=meteo_data_downloader(location,year=2020)



General Workflow
===========

1. Read Location name and position (within Germany)
2. For each Type (wind, solar, pressure) of data, search if the location exists or pick the closest one
3. Build the URL based on Location code and Location Start-End
4. Download the URL
5. Read the data and parse it as DF. Filter the required columns
6. Repeat 2-5 for each type (wind, solar, pressure)
	6.1. For solar, get also DNI data through calculations.
7. Merge the dataframes and write to CSV
8. Add a comment with data description
9. Write the data to the desired path
10. Delete temporal downloaded and extracted files. 


